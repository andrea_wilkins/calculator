//
//  ViewController.swift
//  MyCalculator
//
//  Created by Andrea Wilkins on 2019/10/09.
//  Copyright © 2019 Andrea Wilkins. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let PLUS = 10
    let MINUS = 11
    let MULTIPLY = 12
    let DIVIDE = 13
    let RECALL = 14
    
    @IBOutlet var lblText : UILabel!
    
    @IBOutlet weak var equationText: UILabel!
    
    var num1 : NSInteger = 0
    var num2 : NSInteger = 0
    var operand : NSInteger = 0
    var answer : Double = 0.0
    var pressedNumber : NSInteger = 0
    var equation = ""
    var symbol = "+"
    var memoryNum : NSInteger = 0
    var savedAnswer : NSInteger = 0
    
    var theNumber : String = "0"
    
    var historyItems: [History] = []
    
    @IBAction func calculate(sender : UIButton){
        UIButton.animate(withDuration: 0.2,
            animations: {
                sender.transform = CGAffineTransform(scaleX: 0.80, y: 0.79)
            },
        completion: { finish in
            UIButton.animate(withDuration: 0.2, animations: {
            sender.transform = CGAffineTransform.identity
            })
        })
        
        if operand == RECALL {
            num2 = Int(memoryNum)
        } else {
            num2 = Int(theNumber)!
        }
        
        if operand == PLUS {
            symbol = "+"
            answer = Double(num1 + num2)
            savedAnswer = Int(answer)
        }
        if operand == MINUS{
            symbol = "-"
            answer = Double(num1 - num2)
            savedAnswer = Int(answer)
        }
        if operand == MULTIPLY{
            symbol = "x"
            answer = Double(num1 * num2)
            savedAnswer = Int(answer)
        }
        if operand == DIVIDE{
            symbol = "÷"
            if num2 == 0 {
                
                let alert = UIAlertController(title: "Error",
                    message: "Cannot divide by zero",
                    preferredStyle: .alert)
                
                let cancelAction = UIAlertAction(title: "Cancel",
                                                 style: .cancel, handler: nil)
                
                alert.addAction(cancelAction)
                
                present(alert, animated: true)
                
            }
            else {
                answer = Double(num1) / Double(num2)
                savedAnswer = Int(answer)
            }
        }
        
        equation = "\(num1) \(symbol) \(num2)"
        
        equationText.text = equation
        
        saveAnswer(equation: equation, answer: answer)
        
        num1 = 0
        num2 = 0
        operand = PLUS
        theNumber = String(answer)
        printNumber()
        
        answer = 0.0
        theNumber = "0"
    }
    
    @IBAction func addToMemory(_ sender: UIButton) {
        UIButton.animate(withDuration: 0.2,
            animations: {
                sender.transform = CGAffineTransform(scaleX: 0.80, y: 0.79)
            },
        completion: { finish in
            UIButton.animate(withDuration: 0.2, animations: {
            sender.transform = CGAffineTransform.identity
            })
        })
        
        memoryNum = savedAnswer
    }
    
    @IBAction func clearMemory(_ sender: UIButton) {
        UIButton.animate(withDuration: 0.2,
            animations: {
                sender.transform = CGAffineTransform(scaleX: 0.80, y: 0.79)
            },
        completion: { finish in
            UIButton.animate(withDuration: 0.2, animations: {
            sender.transform = CGAffineTransform.identity
            })
        })
        memoryNum = 0;
    }
    
    @IBAction func recallMemory(_ sender: UIButton) {
        UIButton.animate(withDuration: 0.2,
            animations: {
                sender.transform = CGAffineTransform(scaleX: 0.80, y: 0.79)
            },
        completion: { finish in
            UIButton.animate(withDuration: 0.2, animations: {
            sender.transform = CGAffineTransform.identity
            })
        })
        theNumber = String(memoryNum)
        printNumber()
    }
    
    @IBAction func setOperand(sender : UIButton){
        UIButton.animate(withDuration: 0.2,
            animations: {
                sender.transform = CGAffineTransform(scaleX: 0.80, y: 0.79)
            },
        completion: { finish in
            UIButton.animate(withDuration: 0.2, animations: {
            sender.transform = CGAffineTransform.identity
            })
        })
        
        if sender.tag >= 10 && sender.tag <= 13{
            operand = sender.tag
            saveNum1()
        }
        
        if sender.tag == -2 {
            theNumber = "0";
            equationText.text = ""
            printNumber()
        }
        
        if sender.tag == 14 {
            num1 = Int(memoryNum)
            theNumber = "0"
            printNumber()
        }
    }
    
    func saveNum1(){
        num1 = Int(theNumber)!
        theNumber = "0"
        printNumber()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        printNumber()
    }
    
    func printNumber(){
        lblText.text = theNumber
    }
    
    
    @IBAction func pressNum(sender : UIButton){
        UIButton.animate(withDuration: 0.2,
            animations: {
                sender.transform = CGAffineTransform(scaleX: 0.80, y: 0.79)
            },
        completion: { finish in
            UIButton.animate(withDuration: 0.2, animations: {
            sender.transform = CGAffineTransform.identity
            })
        })
        
        if sender.tag >= 0 && sender.tag <= 9 {
            if (theNumber == "0") {
                pressedNumber = sender.tag
                theNumber = ""
            }
            
            theNumber += String(sender.tag)
            printNumber()
        }
    }
    
    @IBAction func onHistoryTap(_ sender: UITapGestureRecognizer) {
        performSegue(withIdentifier: "toHistory", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is HistoryViewController {
            let viewController = segue.destination as? HistoryViewController
            viewController?.historyItems = self.historyItems
        }
    }

}

extension ViewController {
    func saveAnswer(equation: String, answer: Double) {
        let history = History()
        
        history.equation = equation
        history.answer = answer
        
        historyItems.append(history)
    }
}


//
//  HistoryViewController.swift
//  MyCalculator
//
//  Created by Andrea Wilkins on 2019/10/13.
//  Copyright © 2019 Andrea Wilkins. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    let identifier = "cell"
    var historyItems: [History] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.reloadData()
    }
    
    @IBAction func onDoneTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClearTap(_ sender: Any) {
        historyItems.removeAll()
        tableView.reloadData()
        
        if let viewController = self.presentingViewController as? ViewController {
            DispatchQueue.main.async {
                viewController.historyItems.removeAll()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
}

// MARK: tableView Delegate & DataSource
extension HistoryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return historyItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        
        let equation = historyItems[indexPath.row]
        cell.textLabel?.text = String(format:"%.1f", equation.answer ?? 0.0)
        cell.detailTextLabel?.text = equation.equation
        
        return cell
    }
}

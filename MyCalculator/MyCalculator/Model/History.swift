//
//  History.swift
//  MyCalculator
//
//  Created by Andrea Wilkins on 2019/10/13.
//  Copyright © 2019 Andrea Wilkins. All rights reserved.
//

import Foundation

class History {
    var equation: String? = ""
    var answer: Double? = 0.0
}

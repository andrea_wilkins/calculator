# Calculate

### What is Calculate?

Calculate is a simple iOS calculator application. 
The user can perform basic addition, subtraction, multiplication and division calculations.
There is also a built in memory recall function and every calculation gets saved to a log on a seperate page.

### Getting Started

* Clone the repository to your local machine.
* Open the repository in Xcode. 
* If your device asks you to update some Xcode packages, make sure that you update them and restart Xcode.
* Once you have the project open, change the device to an iPhone 8 before running the project.
* Click the play button in the top left hand corner to run the project.

### Tech

Speak was created using:

* [Swift] - For the front-end and business logic of the application.


   [Swift]: <https://swift.org/documentation/>
   
![alt Calculate](images/calculate.png)

